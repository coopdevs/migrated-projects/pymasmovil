# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [0.0.21] - 2022-01-21
### Added
- [#116](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/116) Allow to filter out KO order items according to their status when retrieving them by ICC

### Fixed
- [#118](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/118) Do not try to fill contract dictionary parameters if they are empty in the original MM instances.
- [#117](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/117) Filter out order items without `SimAttributes` parameter before filtering by ICC.

## [0.0.20] - 2021-10-26
### Added
- [#114](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/114) Add promotions attribute in Contract
- [#113](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/113) Allow removing promotions in asset's `update_product` method.

## [0.0.19] - 2021-10-20
### Added
- [#111](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/111) Return the newest order-item if a list of them is obtained with the same account and ICC (Copy back changes from MR #102 that were removed in #103 and #104).

## [0.0.18] - 2021-10-07
### Fixed
- [#109](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/109) Set new Contract attributes when instantiating instead of modifying the class ones and assigning them to the new instance.

## [0.0.17] - 2021-09-20
### Removed
- [#107](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/107) Remove TariffChangeRequiredParamsError exception

## [0.0.16] - 2021-09-10
### Removed
- [#104](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/104) Remove unused method `_get_newest_order_item` remaining from MR #101.
- [#103](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/103) Revert MR #102 and #101

## [0.0.15] - 2021-09-09
### Added
- [#101](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/101) Allow `Account.get_order_item_by_ICC` to filter by their status.
### Fixed
- [#102](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/102) Return the newest order-item if a list of them is obtained with the same account and ICC.

## [0.0.14] - 2021-08-06
### Added
- [#97](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/97) Allow using `promotions` in `Asset.update_product` method.
### Fixed
- [#96](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/96) Check that `additionalBond` exists before iterating over it when building a Contract.
### Deleted
- [#98](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/98) Delete unnecessary piece of code in `Asset._build_active_change()` method.
## [0.0.13] - 2021-07-29
### Added
- [#93](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/93) Add MM incoming `additionalBonds`  field to be included in our builded contracts.
- [#89](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/89), [#92](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/92) Allow using `removeBonds` in `Asset.update_product` method.
- [#87](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/87), [#90](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/90) Add MM incoming portability date field "Fecha_planificada_de_entrega" to be included in our builded order-items.
- [#86](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/86) Add method `Account.get_order_item_by_ISCC()` to retrieve an existing order-item from a given account filtering by the ISCC.
### Fixed
- [#94](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/94) Fix expected "OK" message from `Asset.update_product`.
### Changed
- [#85](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/85) Remove "portabilityDate" as order-item creation input
-  [#91](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/91) Do not get the Asset after modifying it with method "update_product", changes aren't applied right after.

## [0.0.12] - 2021-05-27
### Fixed
- [#81](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/81) Update create account paramether requirements

## [0.0.11] - 2021-05-17
### Changed
- [#78](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/78) Update MM endpoints from api version v1.0.0

## [0.0.10] - 2021-04-28
### Added
- [#77](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/77) Add method `Account.get_by_NIF()` to retrieve an existing account filtering by its document number (update in MM API version 0.1.0).

## [0.0.9] - 2020-10-22
### Added
- [#76](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/76) Add method "Asset.update_product", allowing to change the tariffs and add one shot bonds to existing contracts in the MM platform.
- [#75](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/75) Allow HTTP PATCH requests.
- [#74](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/74) Add method `Asset.get_by_phone()` to retrieve an existing asset filtering by its phone number.

## [0.0.8] - 2020-10-09
### Fixed
- [#72](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/72), [#73](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/73) Fix how we build order-items and assets from the response of `Contract.get()`.

## [0.0.7] - 2020-09-23
### Changed
- [#71](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/71) Load domain parameter to login into a MM session from ENVARS. Its value varies between test and production environments.
### Added
- [#70](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/70) Add `Account.get_asset_by_phone()` method.
- [#69](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/69) Add `Asset` model, which has new class `Contract` as parent. `OrderItem` also inherits from `Contract` now, since they both share almost the same class structure.

## [0.0.6] - 2020-09-21
### Fixed
- [#67](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/67), [#68](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/67) Fix `IncompletePortabilityDataException`, now renamed as `NewLineRequestRequiredParamsError`, to allow new phone number registrations as well as portabilities. Also, refactor it together with `AccountRequiredParamsErrorUpdate` to make their logic and structure match.

## [0.0.5] - 2020-09-15
### Added
- [#65](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/65) Add `IncompletePortabilityDataException` to check that required attributes from portabilities are present in the requests before sending them to MM.
- [#64](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/64) Add method to get order-items from accounts filtering by phone number.
- [#61](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/61) Rename  `AutenthicationError` to `MissingLoginCredentialsError`. Add a new `AutenthicationError` exception which is raised when the MM session login fails.

### Fixed
- [#63](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/63) Correct the `documentType` values when checked to build an account.

## [0.0.4] - 2020-09-03
### Added
- [#58](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/58) Improve managing of the different HTTP error estructures coming from the API of MM.

### Fixed
- [#59](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/59) Fix KeyError exceptions in the 'NewLineRequest._to_order_item()' and complete the structure it returns.

## [0.0.3] - 2020-07-27
### Added
- [#54](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/54) Build OrderItem without portabilityDate attribute.
- [#52](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/52) Add required account paramether restriction, and AccountRequiredParamsError.

### Fixed
- [#55](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/55) Fix expected HTTP error structure from the API of MM.
- [#53](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/53) Fix OrderItem.route with correct baseurl path.
- [#50](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/50) Fix dysfunctional syntax in checking conditions from OrderItem constructor.
- [#49](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/49) Fix publish CI job rules.

## [0.0.2] - 2020-04-09
### Added
- [#43](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/43) Raise AuthenticationError when login credentials missing.
### Changed
- [#41](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/41) Disable HTTP requests in tests.

## [0.0.1] - 2020-04-01
### Added
- [#34](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/34) Manage HTTP errors resulting from unsuccessful API requests.
- [#33](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/33) Add CHANGELOG.
- [#30](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/30) Add new order-items to existing accounts.
- [#23](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/23) Retrieve existing order-items.
- [#20](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/20), [#32](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/32) Add README with guidance about package usage.
- [#19](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/19), [#22](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/22) Return new account instance when creating new accounts.
- [#16](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/16) Send actual session to API calls to check that user is correctly logged in.
- [#15](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/15) Create new accounts.
- [#13](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/13) Retrieve existing accounts.
- [#12](https://gitlab.com/coopdevs/pymasmovil/-/merge_requests/12) Logging into a session (authentication required) to access Más Móvil's B2B REST API.
