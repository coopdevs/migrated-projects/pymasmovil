import unittest2 as unittest

from pymasmovil.models.new_line_request import NewLineRequest


class NewLineRequestTests(unittest.TestCase):
    def setUp(self):

        self.account_id = "0000001"
        self.new_line_request = {
            "lineInfo": [
                {
                    "idLine": 1,
                    "name": "John",
                    "surname": "Doe",
                    "documentType": "VAT",
                    "docid": "12345678A",
                    "phoneNumber": "666666666",
                    "iccid": "3333333",
                    "iccid_donante": "255525",
                    "donorOperator": "Orange",
                    "productInfo": {"id": "1111111111111", "additionalBonds": []},
                }
            ]
        }

    def test_to_order_item(self):

        new_line_request = NewLineRequest(self.new_line_request, self.account_id)

        mapped_order_item = new_line_request.to_order_item()

        self.assertEqual(mapped_order_item["name"], "John")
        self.assertEqual(mapped_order_item["phone"], "666666666")
        self.assertEqual(mapped_order_item["productName"], "Unknown")
        self.assertEqual(
            mapped_order_item["attributes"]["Numero_de_Documento"], "12345678A"
        )
        self.assertEqual(
            mapped_order_item["attributes"]["Operador_Donante_Movil"], "Orange"
        )
        self.assertEqual(mapped_order_item["simAttributes"]["ICCID"], "3333333")
