import unittest2 as unittest
from mock import patch, Mock

from pymasmovil.models.session import Session
from pymasmovil.models.asset import Asset
from pymasmovil.errors.exceptions import UnknownMMError


class AssetTests(unittest.TestCase):
    def setUp(self):
        self.session = Session("sample-session-id")
        self.expected_id = "0000001"
        self.expected_route = Asset._route
        self.phone = "691201759"
        self.expected_got_asset = {
            "asset": {
                "id": self.expected_id,
                "productId": "fake-product-code"
            }
        }
        self.expected_asset_got_by_phone = {
            "rows": [
                {
                    "assetType": "Movil",
                    "id": self.expected_id,
                    "name": "Lladruc",
                    "phone": self.phone,
                    "productName": "Tarifa Ilimitada CM Voz Total Nov19",
                    "status": "En progreso",
                    "surname": "Test,Prova",
                }
            ]
        }
        self.change_tariff_route = "/v1/assets/{}/change-asset".format(self.expected_id)
        self.transaction_id = "00001"
        self.product_id = "AAAAAAAA"
        self.date = "2020-10-19"
        self.additional_bonds = [
            {"id": "A01", "qty": 1},
            {"id": "A02", "qty": 2},
        ]
        self.expected_active_change = {
            "transactionId": self.transaction_id,
            "assetInfo": {
                "executeDate": self.date,
                "productId": self.product_id,
                "additionalBonds": self.additional_bonds,
                "removeBonds": [],
                "promotions": [],
                "removePromotions": [],
            },
        }
        self.expected_ok_response = "Petición aceptada"

    @patch("pymasmovil.models.contract.Client", return_value=Mock(spec=["get"]))
    def test_get(self, MockClient):
        expected_get_route = "{}/{}".format(self.expected_route, self.expected_id)

        def client_get_side_effect(route):
            if route == expected_get_route:
                return self.expected_got_asset

        MockClient.return_value.get.side_effect = client_get_side_effect

        asset = Asset.get(self.session, self.expected_id)

        self.assertIsInstance(asset, Asset)
        self.assertEqual(asset.__dict__, self.expected_got_asset["asset"])

    def test_additionalBonds_structure(self):

        asset_body = {
            "id": self.expected_id,
            "additionalBonds": [
                {
                    "id": "1",
                    "Name": "first",
                    "ExternalIDXenaRC": "A1",
                    "AssetId": "AAAA1"
                },
                {
                    "id": "2",
                    "Name": "second",
                    "ExternalIDXenaRC": "A2",
                    "AssetId": "AAAA2"
                },
            ]
        }
        asset = Asset(**asset_body)

        self.assertIsInstance(asset, Asset)
        self.assertEqual(len(asset.additionalBonds), 2)
        self.assertEqual(asset.additionalBonds, asset_body.get("additionalBonds"))

    @patch("pymasmovil.models.asset.Client")
    def test_get_asset_by_phone(self, MockClient):

        expected_params = {"rowsPerPage": 1, "actualPage": 1, "phone": self.phone}

        def client_get_asset_by_phone_side_effect(route, **params):
            if route == self.expected_route and params == expected_params:
                return self.expected_asset_got_by_phone

        MockClient.return_value = Mock(spec=["get"])
        MockClient.return_value.get.side_effect = client_get_asset_by_phone_side_effect

        asset = Asset.get_by_phone(self.session, self.phone)

        self.assertIsInstance(asset, Asset)
        self.assertEqual(asset.__dict__, self.expected_asset_got_by_phone["rows"][0])

    @patch("pymasmovil.models.asset.Client", return_value=Mock(spec=["patch"]))
    def test_change_tariff(self, MockClient_asset):

        def client_patch_side_effect(route, params, body):
            if (
                route == self.change_tariff_route
                and body == self.expected_active_change
            ):
                return self.expected_ok_response

        MockClient_asset.return_value.patch.side_effect = client_patch_side_effect

        response = Asset.update_product(
            self.session,
            self.expected_id,
            transaction_id=self.transaction_id,
            product_id=self.product_id,
            execute_date=self.date,
            additional_bonds=self.additional_bonds,
        )
        self.assertEqual(response, self.expected_ok_response)

    @patch("pymasmovil.models.asset.Client", return_value=Mock(spec=["patch"]))
    def test_change_bonds_remove_one(self, MockClient_asset):

        bonds_to_remove = ["11111X"]
        self.expected_active_change["assetInfo"].update({
            "removeBonds": [{"assetId": bonds_to_remove[0]}],
            "productId": "",
        })

        def client_patch_side_effect(route, params, body):
            if (
                route == self.change_tariff_route
                and body == self.expected_active_change
            ):
                return self.expected_ok_response

        MockClient_asset.return_value.patch.side_effect = client_patch_side_effect

        response = Asset.update_product(
            self.session,
            self.expected_id,
            transaction_id=self.transaction_id,
            execute_date=self.date,
            additional_bonds=self.additional_bonds,
            bonds_to_remove=bonds_to_remove
        )

        self.assertEqual(response, self.expected_ok_response)

    @patch("pymasmovil.models.asset.Client", return_value=Mock(spec=["patch"]))
    def test_change_bonds_remove_more_than_one(self, MockClient_asset):

        bonds_to_remove = ["11111X", "22222X"]
        self.expected_active_change["assetInfo"].update({
            "removeBonds": [{"assetId": bond} for bond in bonds_to_remove],
            "productId": ""
        })

        def client_patch_side_effect(route, params, body):
            if (
                route == self.change_tariff_route
                and body == self.expected_active_change
            ):
                return self.expected_ok_response

        MockClient_asset.return_value.patch.side_effect = client_patch_side_effect

        response = Asset.update_product(
            self.session,
            self.expected_id,
            transaction_id=self.transaction_id,
            execute_date=self.date,
            additional_bonds=self.additional_bonds,
            bonds_to_remove=bonds_to_remove
        )

        self.assertEqual(response, self.expected_ok_response)

    @patch("pymasmovil.models.asset.Client", return_value=Mock(spec=["patch"]))
    def test_change_tariff_with_promotion(self, MockClient_asset):

        promotion = {"id": "P01", "qty": 1}
        self.expected_active_change["assetInfo"].update({
            "promotions": [promotion],
            "productId": ""
        })

        def client_patch_side_effect(route, params, body):
            if (
                route == self.change_tariff_route
                and body == self.expected_active_change
            ):
                return self.expected_ok_response

        MockClient_asset.return_value.patch.side_effect = client_patch_side_effect

        response = Asset.update_product(
            self.session,
            self.expected_id,
            transaction_id=self.transaction_id,
            execute_date=self.date,
            additional_bonds=self.additional_bonds,
            promotions=[promotion],
        )

        self.assertEqual(response, self.expected_ok_response)

    @patch("pymasmovil.models.asset.Client", return_value=Mock(spec=["patch"]))
    def test_tariff_change_promotions_to_remove(self, MockClient_asset):

        promotions_to_remove = ["11111X", "22222X"]
        self.expected_active_change["assetInfo"].update({
            "removePromotions": [{"assetId": bond} for bond in promotions_to_remove],
            "productId": ""
        })

        def client_patch_side_effect(route, params, body):
            if (
                route == self.change_tariff_route
                and body == self.expected_active_change
            ):
                return self.expected_ok_response

        MockClient_asset.return_value.patch.side_effect = client_patch_side_effect

        response = Asset.update_product(
            self.session,
            self.expected_id,
            transaction_id=self.transaction_id,
            execute_date=self.date,
            additional_bonds=self.additional_bonds,
            promotions_to_remove=promotions_to_remove
        )

        self.assertEqual(response, self.expected_ok_response)

    @patch("pymasmovil.models.asset.Client", return_value=Mock(spec=["patch"]))
    def test_change_tariff_KO(self, MockClient_asset):

        expected_response = "This is a test error message"

        def client_patch_side_effect(route, params, body):
            if (
                route == self.change_tariff_route
                and body == self.expected_active_change
            ):
                return expected_response

        MockClient_asset.return_value.patch.side_effect = client_patch_side_effect

        self.assertRaisesRegex(
            UnknownMMError,
            expected_response,
            Asset.update_product,
            self.session,
            self.expected_id,
            product_id=self.product_id,
            transaction_id=self.transaction_id,
            execute_date=self.date,
            additional_bonds=self.additional_bonds,
        )

    @patch("pymasmovil.models.asset.Client", return_value=Mock(spec=["patch"]))
    def test_add_one_shot(self, MockClient_asset):

        self.expected_active_change["assetInfo"].update({
            "executeDate": "", "productId": ""
        })

        def client_patch_side_effect(route, params, body):
            if (
                route == self.change_tariff_route
                and body == self.expected_active_change
            ):
                return self.expected_ok_response

        MockClient_asset.return_value.patch.side_effect = client_patch_side_effect

        response = Asset.update_product(
            self.session,
            self.expected_id,
            transaction_id=self.transaction_id,
            additional_bonds=self.additional_bonds,
        )

        self.assertEqual(response, self.expected_ok_response)
