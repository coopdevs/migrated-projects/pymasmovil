import unittest2 as unittest
import json

from mock import patch
from pymasmovil.client import Client
from pymasmovil.models.session import Session
from pymasmovil.errors.http_error import HTTPError
from tests.fake_response import FakeResponse


@patch.dict("os.environ", {"MM_BASEURL": "base-url"})
class ClientTests(unittest.TestCase):
    def setUp(self):
        self.expected_route = "/path"
        self.expected_params = {"example_param": "value"}
        self.expected_body = {"example_body": "value"}
        self.expected_headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
        }

        self.sample_session = Session("sample-session-id")
        self.expected_headers_auth = self.expected_headers.copy()
        self.expected_headers_auth["Authorization"] = "Bearer {}".format(
            self.sample_session.session_id
        )

        self.mm_error = {
            "errors": [
                {
                    "statusCode": 400,
                    "fields": "name",
                    "message": "wrong name",
                }
            ]
        }
        self.http_error_response = FakeResponse(400, self.mm_error)

    @patch("pymasmovil.client.requests.request", return_value=FakeResponse(200))
    def test_get(self, mock_request):

        Client().get(self.expected_route, example_param="value")

        mock_request.assert_called_once_with(
            "GET",
            "base-url" + self.expected_route,
            data=None,
            params=self.expected_params,
            headers=self.expected_headers,
        )

    @patch("pymasmovil.client.requests.request", return_value=FakeResponse(200))
    def test_get_auth(self, mock_request):

        Client(self.sample_session).get(self.expected_route, example_param="value")

        mock_request.assert_called_once_with(
            "GET",
            "base-url" + self.expected_route,
            data=None,
            params=self.expected_params,
            headers=self.expected_headers_auth,
        )

    @patch("pymasmovil.client.requests.request", return_value=FakeResponse(200))
    def test_post(self, mock_request):
        Client().post(self.expected_route, self.expected_params, self.expected_body)

        mock_request.assert_called_once_with(
            "POST",
            "base-url" + self.expected_route,
            data=json.dumps(self.expected_body),
            params=self.expected_params,
            headers=self.expected_headers,
        )

    @patch("pymasmovil.client.requests.request", return_value=FakeResponse(200))
    def test_post_auth(self, mock_request):
        Client(self.sample_session).post(
            self.expected_route, self.expected_params, self.expected_body
        )

        mock_request.assert_called_once_with(
            "POST",
            "base-url" + self.expected_route,
            data=json.dumps(self.expected_body),
            params=self.expected_params,
            headers=self.expected_headers_auth,
        )

    @patch("pymasmovil.client.requests.request", return_value=FakeResponse(200))
    def test_patch_auth(self, mock_request):
        Client(self.sample_session).patch(
            self.expected_route, self.expected_params, self.expected_body
        )

        mock_request.assert_called_once_with(
            "PATCH",
            "base-url" + self.expected_route,
            data=json.dumps(self.expected_body),
            params=self.expected_params,
            headers=self.expected_headers_auth,
        )

    @patch("pymasmovil.client.requests.request")
    def test_get_http_error(self, mock_request):

        mock_request.return_value = self.http_error_response
        auth_client = Client(self.sample_session)

        self.assertRaises(HTTPError, auth_client.get, self.expected_route)

    @patch("pymasmovil.client.requests.request")
    def test_post_http_error(self, mock_request):

        mock_request.return_value = self.http_error_response
        auth_client = Client(self.sample_session)

        self.assertRaises(
            HTTPError,
            auth_client.post,
            self.expected_route,
            self.expected_params,
            self.expected_body,
        )

    @patch("pymasmovil.client.requests.request")
    def test_patch_http_error(self, mock_request):

        mock_request.return_value = self.http_error_response
        auth_client = Client(self.sample_session)

        self.assertRaises(
            HTTPError,
            auth_client.patch,
            self.expected_route,
            self.expected_params,
            self.expected_body,
        )
