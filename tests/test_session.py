import unittest2 as unittest
from mock import patch, Mock

from pymasmovil.models.session import Session
from pymasmovil.errors.exceptions import (
    AutenthicationError,
    MissingLoginCredentialsError,
)


class SessionTests(unittest.TestCase):
    @patch.dict(
        "os.environ",
        {
            "MM_USER": "sample-user",
            "MM_PASSWORD": "sample-password",
            "MM_DOMAIN": "test",
        },
    )
    @patch("pymasmovil.models.session.Client")
    def test_create_ok(self, MockClient):

        expected_token = "one-session-id"
        expected_route = "/v2/login-api"
        expected_params = {
            "domain": "test",
        }
        expected_response = {"sessionId": expected_token}
        expected_body = {
            "username": "sample-user",
            "password": "sample-password",
        }

        def mock_create_session(route, params, body):
            if (
                route == expected_route
                and params == expected_params
                and body == expected_body
            ):
                return expected_response

        MockClient.return_value = Mock(spec=["post"])
        MockClient.return_value.post.side_effect = mock_create_session

        session = Session.create()

        self.assertEqual(session.session_id, expected_token)

    @patch.dict(
        "os.environ",
        {
            "MM_USER": "sample-user",
            "MM_PASSWORD": "sample-password",
            "MM_DOMAIN": "test",
        },
    )
    @patch("pymasmovil.models.session.Client")
    def test_create_login_fail(self, MockClient):

        expected_response = {"sessionId": ""}

        MockClient.return_value = Mock(spec=["post"])
        MockClient.return_value.post.return_value = expected_response

        self.assertRaises(AutenthicationError, Session.create)

    @patch.dict(
        "os.environ",
        {
            "MM_USER": "sample-user",
            "MM_PASSWORD": "sample-password",
            "MM_DOMAIN": "test",
        },
    )
    def test_create_missing_baseURL(self):

        self.assertRaisesRegex(
            MissingLoginCredentialsError, "MM_BASEURL", Session.create
        )

    @patch.dict("os.environ", {"MM_PASSWORD": "sample-password", "MM_DOMAIN": "test"})
    def test_create_missing_credentials(self):

        self.assertRaisesRegex(MissingLoginCredentialsError, "MM_USER", Session.create)
